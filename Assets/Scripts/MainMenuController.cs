﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    #region Fields

    // SETTINGS //
    public Vector2 layer1ParallaxRange;
    public Vector2 layer2ParallaxRange;
    public Vector2 layer3ParallaxRange;
    public Vector2 layer4ParallaxRange;
    public Vector2 layer5ParallaxRange;

    public float lerpSpeed = 1;

    // REFERENCES //
    RectTransform layer1;
    RectTransform layer2;
    RectTransform layer3;
    RectTransform layer4;
    RectTransform layer5;

    #endregion

    #region Unity Functions

    public void Awake()
    {
        layer1 = transform.Find("Layer1").GetComponent<RectTransform>();
        layer2 = transform.Find("Layer2").GetComponent<RectTransform>();
        layer3 = transform.Find("Layer3").GetComponent<RectTransform>();
        layer4 = transform.Find("Layer4").GetComponent<RectTransform>();
        layer5 = transform.Find("Layer5").GetComponent<RectTransform>();
    }

    public void Update()
    {
        float xPercentage = Input.mousePosition.x / Screen.width;
        float yPercentage = Input.mousePosition.y / Screen.height;

        layer1.localPosition = Vector2.Lerp(layer1.localPosition, new Vector2(119 + Mathf.Lerp(-layer1ParallaxRange.x, layer1ParallaxRange.x, xPercentage), 71 + Mathf.Lerp(-layer1ParallaxRange.y, layer1ParallaxRange.y, yPercentage)), lerpSpeed * Time.deltaTime);
        layer2.localPosition = Vector2.Lerp(layer2.localPosition, new Vector2(Mathf.Lerp(-layer2ParallaxRange.x, layer2ParallaxRange.x, xPercentage), Mathf.Lerp(-layer2ParallaxRange.y, layer2ParallaxRange.y, yPercentage)), lerpSpeed * Time.deltaTime);
        layer3.localPosition = Vector2.Lerp(layer3.localPosition, new Vector2(Mathf.Lerp(-layer3ParallaxRange.x, layer3ParallaxRange.x, xPercentage), Mathf.Lerp(-layer3ParallaxRange.y, layer3ParallaxRange.y, yPercentage)), lerpSpeed * Time.deltaTime);
        layer4.localPosition = Vector2.Lerp(layer4.localPosition, new Vector2(Mathf.Lerp(-layer4ParallaxRange.x, layer4ParallaxRange.x, xPercentage), Mathf.Lerp(-layer4ParallaxRange.y, layer4ParallaxRange.y, yPercentage)), lerpSpeed * Time.deltaTime);
        layer5.localPosition = Vector2.Lerp(layer5.localPosition, new Vector2(Mathf.Lerp(-layer5ParallaxRange.x, layer5ParallaxRange.x, xPercentage), Mathf.Lerp(-layer5ParallaxRange.y, layer5ParallaxRange.y, yPercentage)), lerpSpeed * Time.deltaTime);

        //Debug.Log(layer1.position);
    }

    #endregion

    #region Functions

    public void StartGame()
    {
        SceneManager.LoadScene("Play", LoadSceneMode.Single);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void LoadIntro()
    {
        SceneManager.LoadScene("Intro", LoadSceneMode.Single);
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("Main Menu", LoadSceneMode.Single);
    }

    public void LoadCredits()
    {
        SceneManager.LoadScene("Credits", LoadSceneMode.Single);
    }

    public void OpenConceptArt()
    {
        Application.OpenURL("http://imgur.com/a/14Kcv");
    }

    #endregion
}
