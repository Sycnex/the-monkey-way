﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{
    #region Fields

    // SETTTINGS //
    public float rotationSpeed;
    public float rotationRange;
    public float acceleration;
    public float deceleration;
    public float backwardsForceZLimit;
    public float maxBackwardsForce;
    public float middleDragMaxYForce = 10;
    public float middleDragYForceChangeRate = 1;
    public float asteroidSpeedBoostMult = 5f;
    public float mouseYMovementAcceleration = 1;
    public float timer;

    public float maxXMovementForce;
    public float maxYMovementForce;

    public GameObject debrisPrefab;

    // VARIABLES //
    private Vector3 startPosition;
    private Vector3 backwardsForce;
    private Vector3 movementForce;
    private float middleDragYForce; // The force that drags the player back into the middle of the map.
    [System.NonSerialized]
    public Vector3 attractionForce;
    private float asteroidBoostForce;
    float mouseRotation;

    // REFERENCES //
    Rigidbody rigidbody;
    Attractor attractor;
    PlayerCharger charger;

    #endregion

    #region Unity Functions

    private void Awake()
    {
        // GATHER REFERENCES //
        rigidbody = GetComponent<Rigidbody>();
        attractor = GetComponent<Attractor>();
        charger = GetComponent<PlayerCharger>();

        // INITIALIZE //
        startPosition = transform.position;
    }

    private void Update()
    {
        LookToMouse();
        Movement();
        ApplyMovement();
        //Debug.Log(rigidbody.velocity);
    }

    #endregion

    #region Functions

    private void OnCollisionStay() {
        timer += Time.deltaTime;

        if (timer > 1.0) // #ruthless
        {
            charger.SubtractCharge(100);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject.Instantiate(debrisPrefab, collision.contacts[0].point, Quaternion.LookRotation(collision.contacts[0].normal)).name = "Ragdoll";
        Camera.main.GetComponent<CameraController>().Shake(0.5f, 0.1f);
    }

    private void LookToMouse()
    {
        Plane playerPlane = new Plane(Vector3.right, transform.position);

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        float hitdist = 0.0f;

        if (playerPlane.Raycast(ray, out hitdist))
        {
            Vector3 targetPoint = ray.GetPoint(hitdist);

            Vector3 targetRotation = Quaternion.LookRotation(targetPoint - transform.position).eulerAngles;

            mouseRotation = targetRotation.x;

            if (mouseRotation > rotationRange && mouseRotation < 180)
            {
                mouseRotation = rotationRange;
            }

            if (mouseRotation < 360 - rotationRange && mouseRotation > 180)
            {
                mouseRotation = 360 - rotationRange;
            }

            targetRotation = new Vector3(mouseRotation, transform.rotation.y, transform.rotation.z);

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(targetRotation), rotationSpeed * Time.deltaTime);
        }
    }

    private void Movement()
    {
        //Debug.Log(movementForce + ", Magnitude: " + movementForce.magnitude);
        // Forwards movement.
        if (Input.GetKey(KeyCode.W))
        {
            movementForce += transform.forward * acceleration;
        }
        else
        {
            movementForce = movementForce.normalized * Mathf.Max(0, (movementForce.magnitude - (deceleration * Time.deltaTime)));
        }

        if (mouseRotation > 0 && mouseRotation < 180)
        {
            movementForce += (mouseRotation / rotationRange) * mouseYMovementAcceleration * Vector3.down;
        }
        else if (mouseRotation < 360 && mouseRotation > 0)
        {
            movementForce += ((360 - mouseRotation) / rotationRange) * mouseYMovementAcceleration * Vector3.up;
        }

        // Force Limiters
        float forwardsForce = Vector3.Dot(movementForce, Vector3.forward);

        if (Mathf.Abs(forwardsForce) > maxXMovementForce)
        {
            movementForce -= forwardsForce * Vector3.forward;
            movementForce += maxXMovementForce * Mathf.Sign(forwardsForce) * Vector3.forward;
        }

        float upwardsForce = Vector3.Dot(movementForce, Vector3.up);

        if (Mathf.Abs(upwardsForce) > maxYMovementForce)
        {
            movementForce -= upwardsForce * Vector3.up;
            movementForce += maxYMovementForce * Mathf.Sign(upwardsForce) * Vector3.up;
        }
    }

    private void ApplyMovement()
    {
        if (attractionForce != Vector3.zero)
        {
            asteroidBoostForce = attractionForce.magnitude * asteroidSpeedBoostMult;
        }
        else
        {
            // Slowly taper off the boost force.
            asteroidBoostForce = Mathf.Lerp(asteroidBoostForce, 0, 3 * Time.deltaTime);
        }

        rigidbody.velocity = movementForce + attractionForce + (asteroidBoostForce * transform.forward);
        //Debug.Log(asteroidBoostForce);

        attractionForce = Vector3.zero;
    }

    #endregion
}
