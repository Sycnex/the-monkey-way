﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour {

    private AudioSource sourceAmbient;
    private AudioSource sourceDrums;
    private AudioSource sourceSnare;

    private AudioClip ambientSound;
    private AudioClip drums;
    private AudioClip snare;

    private PlayerCharger charger;

    public AudioController()
    {
        
    }

    // Use this for initialization
    void Start ()
    {
        charger = GameObject.Find("Player").GetComponent<PlayerCharger>();
    }   
    
    void Awake()
    {
        ambientSound = Resources.Load<AudioClip>("TMW_AmbienceTrack1");
        drums = Resources.Load<AudioClip>("TMW_Drums1");
        snare = Resources.Load<AudioClip>("TMW_Drums2");
        sourceAmbient = Instantiate(Resources.Load<GameObject>("AudioSource")).GetComponent<AudioSource>();
        sourceDrums = Instantiate(Resources.Load<GameObject>("AudioSource")).GetComponent<AudioSource>();
        sourceSnare = Instantiate(Resources.Load<GameObject>("AudioSource")).GetComponent<AudioSource>();
        SetAmbientVolume(1f);
        SetDrumVolume(0.05f);
        SetSnareVolume(0);
        PlayAmbientSound();
        PlayDrumEffect();
        PlaySnareEffect();
    }
	
	// Update is called once per frame
	void Update () {
        int numAsteroidsOnScreen = GameObject.FindGameObjectsWithTag("Asteroids").Length;
        //Debug.Log("Num asteroids: " + numAsteroidsOnScreen);


        if (numAsteroidsOnScreen == 0)
            {
            SetDrumVolume(0.05f);
            SetSnareVolume(0);
        }
        else if (numAsteroidsOnScreen < 8)
        {
            float volume = ((float)numAsteroidsOnScreen) / 8;
            //Debug.Log("Volume: " + volume);
            SetDrumVolume(volume);
            SetSnareVolume(volume);
        } 
        else
        {
            SetDrumVolume(1f);
            SetSnareVolume(1f);
        }

        /*float distance = charger.GetDistanceToNearestObstacle();
        Debug.Log("Audio distance: " + distance);
        float volume = 1 / (distance - 10);
        if (distance < 40)
        {
            SetDrumVolume(volume);
        }
        if (distance < 40)
        {
            SetSnareVolume(volume);
        }*/
	}

    public void SetAmbientVolume(float volume)
    {
        sourceAmbient.volume = volume;
    }

    public void SetDrumVolume(float volume)
    {
        sourceDrums.volume = volume;
    }

    public void SetSnareVolume(float volume)
    {
        sourceSnare.volume = volume;
    }

    public void PlayDrumEffect()
    {
        sourceDrums.clip = drums;
        sourceDrums.Play();
    }

    public void PlaySnareEffect()
    {
        sourceSnare.PlayOneShot(snare);
        sourceSnare.Play();
    }

    public void PlayAmbientSound()
    {
        sourceAmbient.clip = ambientSound;
        sourceAmbient.Play();
    }

}
