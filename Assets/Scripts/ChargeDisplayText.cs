﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChargeDisplayText : MonoBehaviour
{
    public RectTransform chargeText;
    public Transform playerTransform;

    public Vector3 offset;

    void LateUpdate()
    {
        chargeText.position = Camera.main.WorldToScreenPoint(offset + playerTransform.position);
    }
}
