﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("LoadNextScene", 3f);
	}

    public void LoadNextScene()
    {
        SceneManager.LoadScene(1);
    }
}
