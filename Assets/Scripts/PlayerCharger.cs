﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;

public class PlayerCharger : MonoBehaviour {

    //
    private float current_charge;
    private float max_charge;
    private float charge_consumption;
    private float charge_regeneration;
    private bool isCollecting;

    // 
    private float distance_nearest_obstacle;

    private bool playerDead = false;
    private Rigidbody playerRigidbody;

    public GameObject ragdollPrefab;
    public Transform playerModel;

    public GameObject playerBody;
    VignetteAndChromaticAberration chromatic;
    // stolen from Attractor
    public float radius = 10.0f;

    // Use this for initialization
    void Start () {
        max_charge = 100;
        current_charge = max_charge;
        charge_consumption = 1;         // iterate these
        charge_regeneration = 1;        //

        playerRigidbody = gameObject.GetComponent<Rigidbody>();

        chromatic = GameObject.Find("Main Camera").GetComponent<VignetteAndChromaticAberration>();
        playerModel = transform.Find("PlayerModel");

        InvokeRepeating("CollectChargeIfPossible", 0.1f, 0.2f);
    }

    void Update()
    {
        if (isCollecting)
        {
            chromatic.chromaticAberration = 10;
        }
        else
        {
            chromatic.chromaticAberration = 4;
        }
    }

    // Collects charge from nearby objects
    void CollectChargeIfPossible() {
        isCollecting = false;

        if (!playerDead)
        {
            List<Collider> objects = GetCollidersToChargeFrom();
            CollectCharge(objects);
            SubtractCharge(charge_consumption);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        Obstacle obstacle = collision.collider.GetComponent<Obstacle>();
        if (obstacle != null)
        {
            SubtractCharge(5f);
        }
    }

    public void OnDeath()
    {
        playerDead = true;
        playerBody.transform.parent = null;
        Rigidbody ragdollRigidbody = playerBody.AddComponent<Rigidbody>();
        // The AddComponent<Rigidbody>() method above is returning null, but does add the component successfully.
        //Debug.Log("Rigidbody: " + ragdollRigidbody);
        //Debug.Log("Ragdoll rigidbody velocity: " + ragdollRigidbody.velocity);
        //ragdollRigidbody.velocity = playerRigidbody.velocity;
        GameObject ragdoll = (GameObject)Instantiate(ragdollPrefab, playerModel.position, playerModel.rotation);
        Destroy(playerModel.gameObject);
    }
    
    private void CollectCharge(List<Collider> colliders) {

        int i = 0;

        foreach (Collider collider in colliders)
        {
            float distance = Vector3.Distance(transform.position, collider.gameObject.transform.position);

            // Set distance to nearest obstacle for the audio manager
            if (i==0)
                SetDistanceToNearestObstacle(distance);
            else
            {
                if (distance < distance_nearest_obstacle)
                {
                    SetDistanceToNearestObstacle(distance);
                    //Debug.Log(collider.gameObject.name);
                }
            }
            //Debug.Log(charge_regeneration * (i+1));
            isCollecting = true;
            AddCharge(charge_regeneration*(i+1)); 
            i++;
        }
        //Debug.Log("Nearest neighbour is: " + distance_nearest_obstacle);

    }

    // Takes input as a positive 
    private void AddCharge(float deltaCharge) {
        if (current_charge+deltaCharge <= max_charge)
        {
            current_charge += deltaCharge;
        } else
        {
            current_charge = max_charge;
        }
            
    }

    public void SubtractCharge(float deltaCharge) {
        current_charge -= deltaCharge;
        if (current_charge < 0)
        {
            current_charge = 0;
        }
    }

    public float GetCharge() {
        return current_charge;
    }



    private List<Collider> GetCollidersToChargeFrom() {

        List<Collider> objectsToAttract = new List<Collider>();

        // Get the objects within range
        Collider[] objectsInRange = Physics.OverlapSphere(transform.position, radius);
        foreach (Collider collider in objectsInRange)
        {
            if (collider.gameObject.name != "Player" && collider.GetComponent<Attractor>() != null)
            {
                objectsToAttract.Add(collider);
            }
        }

        return objectsToAttract;
    }

    public float GetDistanceToNearestObstacle() {
        return distance_nearest_obstacle;
    }

    private void SetDistanceToNearestObstacle(float new_nearest_obstacle) {
        distance_nearest_obstacle = new_nearest_obstacle;
    }
}
