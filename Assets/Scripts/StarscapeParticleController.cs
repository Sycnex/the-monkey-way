﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StarscapeParticleController : MonoBehaviour
{
    #region Fields

    // SETTINGS //
    public float layer1Speed = 95;
    public float layer2Speed = 50;
    public float layer3Speed = 15;

    public List<GameObject> debrisPrefabs;

    // VARIABLES //
    public float currentSpeed = 0;
    public float speedLerp = 1;
    public float debrisTimer;

    // REFERENCES //
    public ParticleSystem layer1;
    public ParticleSystem layer2;
    public ParticleSystem layer3;

    GameManager gameManager;

    #endregion

    #region Unity Functions

    private void Awake()
    {
        layer1 = transform.Find("Particle System1").GetComponent<ParticleSystem>();
        layer2 = transform.Find("Particle System2").GetComponent<ParticleSystem>();
        layer3 = transform.Find("Particle System3").GetComponent<ParticleSystem>();

        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        layer1.playbackSpeed = 0.2f;
        layer2.playbackSpeed = 0.2f;
        layer3.playbackSpeed = 0.2f;

        Invoke("SpawnDebris", Random.Range(5, 6));
    }

    private void Update()
    {
        if (gameManager.startCountdownTimer < -1f && Input.GetKey(KeyCode.W))
        {
            currentSpeed = Mathf.Lerp(currentSpeed, 1, speedLerp * Time.deltaTime);
        }
        else
        {
            currentSpeed = Mathf.Lerp(currentSpeed, 0.2f, speedLerp * Time.deltaTime);
        }

        layer1.playbackSpeed = currentSpeed;
        layer2.playbackSpeed = currentSpeed;
        layer3.playbackSpeed = currentSpeed;
    }

    private void SpawnDebris()
    {
        GameObject debris = (GameObject)Instantiate(debrisPrefabs[Random.Range(0, debrisPrefabs.Count)], transform.position + (600 * Vector3.forward) + (300 * Vector3.left), Quaternion.Euler(0, 270, 0), transform);
        debris.GetComponent<Rigidbody>().velocity = Vector3.back * Random.Range(20, 60);
        debris.GetComponent<Rigidbody>().AddTorque(Vector3.up * 5, ForceMode.Force);
        //Debug.Log("SPawned!!");
        Invoke("SpawnDebris", Random.Range(30, 90));
    }

    #endregion
}
