﻿using UnityEngine;
using System.Collections;

public class ParticleAutoDestroyer : MonoBehaviour
{
    ParticleSystem particleSystem;

    private void Awake()
    {
        particleSystem = GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if (particleSystem.isStopped)
        {
            Destroy(gameObject);
        }
    }
}
