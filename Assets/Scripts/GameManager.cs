﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

    public Text timeDisplay;
    public Text chargeDisplay;

    private PlayerCharger player;
    private CameraController cameraController;
    private int playerScore;
    private bool gameIsOver = false;

    public Vector3 offset;
    public Transform cameraTarget;

    public Text startCountdownTimerText;
    public float startCountdownTimer = 3.99f;

    // Use this for initialization
    void Awake () {
        playerScore = 0;
        gameIsOver = false;
        player = GameObject.FindObjectOfType<PlayerCharger>();
        cameraController = Camera.main.GetComponent<CameraController>();
        cameraTarget = player.transform;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (startCountdownTimer > -1f)
        {
            startCountdownTimer -= Time.deltaTime;

            if (startCountdownTimer > 1)
            {
                startCountdownTimerText.text = Mathf.Floor(startCountdownTimer).ToString();
            }
            else
            {
                startCountdownTimerText.text = "GO!";
                startCountdownTimerText.color = new Color(Random.value, Random.value, Random.value);
            }
        }
        else if (startCountdownTimerText.text != "")
        {
            startCountdownTimerText.text = "";
        }

        setScore(Time.timeSinceLevelLoad);
        setCharge(player.GetCharge());

        cameraController.Pan(cameraTarget.transform.position + offset, 1f);

        if (!gameIsOver)
        {
            CheckGameOver();
        }
        
    }

    private void setScore(float x) {
        playerScore = (int) x;
        timeDisplay.text = playerScore.ToString();
    }

    private void CheckGameOver()
    {
        // Access current charge level
        if (player.GetCharge() <= 0 && !gameIsOver)
        {
            Debug.Log("Game Over!");
            gameIsOver = true;
            player.OnDeath();

            Invoke("LoadGameOverScreen", 3f);
            //cameraTarget = GameObject.Find("Ragdoll").transform;
        }
    }

    private void LoadGameOverScreen()
    {
        SceneManager.LoadScene("GameOver");
    }

    private void setCharge(float x) {
        chargeDisplay.text = x.ToString();
    }
}
