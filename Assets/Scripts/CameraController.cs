﻿using UnityEngine;
using System.Collections;

/*  To use:
 *  Add as component to camera 
 *  Call camera animation functions
 *  Different animations can occur simultaneously
 *  Only one instance of an animation type can be active at a time
 *  To prematurely stop an animation instance, call Halt for that particular animation before calling the corresponding animation function
 *  To halt all animation, call HaltAll
 *  To reset the camera to initial default settings, call Reset
 *  To focus on a position or transform, call Focus 
 */


public class CameraController : MonoBehaviour {

    // SETTINGS //
    float minZoom = 1;
    float maxZoom = 100;

    // VARIABLES //
    bool isPan = false;
    Vector3 panTo;
    float panSpeed;

    bool isShake = false;
    float shakeIntensity;
    float shakeDecay;
    Vector3 originPos;
    Quaternion originRot;

    bool isRotate = false;
    float rotAngle;
    float rotSpeed;

    bool isZoom = false;
    float zoomAmount;
    float zoomDuration;

    bool isFocus = false;
    Transform focusTarget;
    Vector3 focusPosition;

    Vector3 defaultPosition;
    float defaultRotation;
    float defaultFov;

    // REFERENCES //
    Camera cam;

    void Start ()
    {
        cam = GetComponent<Camera>();
        defaultPosition = cam.transform.position;
        defaultFov = cam.fieldOfView;
        defaultRotation = cam.transform.rotation.eulerAngles.z;

    }

    void Awake()
    {
        /*  Example use

        Rotate(20, 0.05f);
        Zoom(50, 0.5f);
        Pan(new Vector3(transform.position.x, transform.position.y, transform.position.z + 5), 0.05f);
        Shake(0.3f, 0.01f);
        HaltShake();
        Shake(0.5f, 0.01f);

        */
    }
	
	void Update () {
        isPan = DoPan();
        isShake = DoShake();
        isRotate = DoRotate();
        isZoom = DoZoom();
        isFocus = DoFocus();
        //Debug.Log("isPan = " + isPan + " | isShake = " + isShake + " | isRotate = " + isRotate + " | isZoom = " + isZoom + " | isFocus = " + isFocus);
	}

    public void Shake(float shakeIntensity, float shakeDecay)
    {
        if (isShake != true)
        {
            this.shakeIntensity = shakeIntensity;
            this.shakeDecay = shakeDecay;
            this.originPos = transform.position;
            this.originRot = transform.rotation;
            isShake = true;
        }
        else
        {
            //Debug.Log("Error: Already shaking!");
        }
    }

    public void Pan(Vector3 panTo, float panSpeed)
    {
        if (isPan != true || isFocus == true)
        {
            panTo.x = transform.position.x;
            this.panTo = panTo;
            this.panSpeed = panSpeed;
            isPan = true;
        }
        else
        {
            Debug.Log("Error: Already panning!");
        }
    }

    public void Zoom(float zoomAmount, float zoomDuration)
    {
        if(isZoom != true)
        {
            if (zoomAmount < minZoom)
                zoomAmount = minZoom;
            if (zoomAmount > maxZoom)
                zoomAmount = maxZoom;
            this.zoomAmount = zoomAmount;
            this.zoomDuration = zoomDuration;
            isZoom = true;
        }
        else
        {
            Debug.Log("Error: Already zooming!");
        }
    }

    public void Rotate(float rotAngle, float rotSpeed)
    {
        if (isRotate != true)
        {
            this.rotAngle = rotAngle;
            this.rotSpeed = rotSpeed;
            isRotate = true;
        }
        else
        {
            Debug.Log("Error: Already rotating!");
        }
    }

    public void Focus(Transform focusTarget)
    {
        if(isFocus != true)
        {
            HaltAll();
            this.focusTarget = focusTarget;
            Pan(focusTarget.position, 0.1f);
            Rotate(focusTarget.rotation.eulerAngles.z, 0.01f);
            Zoom(2, 4f);
            isFocus = true;
        }
        else
        {
            Debug.Log("Error: Already focussing on target!");
        }
    }

    public void Focus(Vector3 focusPosition)
    {
        if (isFocus != true)
        {
            HaltAll();
            this.focusPosition = focusPosition;
            Pan(focusPosition, 0.1f);
            Zoom(2, 4f);
            isFocus = true;
        }
        else
        {
            Debug.Log("Error: Already focussing on position!");
        }
    }

    public void HaltAll()
    {
        isPan = false;
        isShake = false;
        isRotate = false;
        isZoom = false;
        isFocus = false;
    }

    public void HaltPan()
    {
        isPan = false;
    }

    public void HaltShake()
    {
        isShake = false;
    }

    public void HaltRotate()
    {
        isRotate = false;
    }

    public void HaltZoom()
    {
        isZoom = false;
    }

    public void HaltFocus()
    {
        isFocus = false;
    }

    public void Reset()
    {
        HaltAll();
        Pan(defaultPosition, 0.2f);
        Rotate(defaultRotation, 0.2f);
        Zoom(100/defaultFov, 1.0f);
    }

    private bool DoPan()
    {
        if(isPan == true)
        {
            transform.position = Vector3.Lerp(transform.position, panTo, panSpeed);
            if (Vector3.Distance(transform.position, panTo) > 0.05)
                return true;
            else
                transform.position = panTo;
        }
        return false;
    }

    private bool DoShake()
    {
        if(isShake == true && shakeIntensity > 0)
        {
            transform.position = originPos + Random.insideUnitSphere * shakeIntensity;
            /*transform.rotation = new Quaternion(
                         originRot.x + Random.Range(-shakeIntensity, shakeIntensity) * .1f,
                         originRot.y + Random.Range(-shakeIntensity, shakeIntensity) * .1f,
                         originRot.z + Random.Range(-shakeIntensity, shakeIntensity) * .1f,
                         originRot.w + Random.Range(-shakeIntensity, shakeIntensity) * .1f);\
                         */
            shakeIntensity -= shakeDecay;
            return true;
        }
        return false;
    }

    private bool DoRotate()
    {
        if(isRotate == true)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, rotAngle)), Time.time * rotSpeed);
            return true;
        }
        return false;
    }

    private bool DoZoom()
    {
        if(isZoom == true)
        {
            cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, maxZoom / zoomAmount, Time.deltaTime * zoomDuration);
            if (Mathf.Abs(cam.fieldOfView - maxZoom / zoomAmount) > 0.05)
                return true;
            else
                cam.fieldOfView = maxZoom / zoomAmount;
        }
        return false;
    }

    private bool DoFocus()
    {
        if(isFocus == true)
        {
            if(focusTarget != null)
            {
                if (isZoom == false && isPan == false && isRotate == false)
                    return false;
                Pan(focusTarget.position, 0.1f);
                Rotate(focusTarget.rotation.eulerAngles.z, 0.01f);
                Zoom(2f, 4f);
            }
            else if(focusPosition != null)
            {
                if (isZoom == false && isPan == false)
                    return false;
                Pan(focusPosition, 0.1f);
                Zoom(2f, 4f);
            }
            return true;
        }
        return false;
    }
}
