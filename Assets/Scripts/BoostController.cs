﻿using UnityEngine;
using System.Collections;

public class BoostController : MonoBehaviour
{
    #region Fields

    // REFERENCES //
    PlayerController playerController;

    Transform boostBallBack;
    Transform boostBallFront;
    Transform boostLine;

    ParticleSystem boostRings;

    float boostScale = 0.7f;

    GameManager gameManager;

    #endregion

    #region Unity Functions

    public void Awake()
    {
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        boostBallBack = transform.Find("BoostBall/BoostBallBack");
        boostBallFront = transform.Find("BoostBall/BoostBallFront");
        boostLine = transform.Find("BoostLine");
        boostRings = transform.Find("BoostLine/Ring").GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if (gameManager.startCountdownTimer < -1f && Input.GetKey(KeyCode.W))
        {
            boostScale = Mathf.Lerp(boostScale, 3f, Time.deltaTime * 6);

            if (boostRings.isStopped)
            {
                boostRings.Play();
            }
        }
        else
        {
            boostScale = Mathf.Lerp(boostScale, 0.9f, Time.deltaTime * 6);

            if (boostRings.isPlaying)
            {
                boostRings.Stop();
            }
        }

        boostBallBack.localScale = new Vector3(boostBallBack.localScale.x, boostBallBack.localScale.y, Random.Range(0.07f, 0.12f) * boostScale);
        boostBallFront.localScale = new Vector3(boostBallBack.localScale.x, boostBallBack.localScale.y, Random.Range(0.07f, 0.12f) * boostScale);
        boostLine.localScale = new Vector3(boostLine.localScale.x, boostScale * 0.4f, boostLine.localScale.z);
    }

    public void LateUpdate()
    {
        boostLine.rotation = Quaternion.Euler(0, 0, 0);
    }

    #endregion
}
