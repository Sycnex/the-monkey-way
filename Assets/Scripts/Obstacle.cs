﻿using UnityEngine;
using System.Collections;

public class Obstacle : MonoBehaviour {

    private Transform player;
    private float playerLength;

	// Use this for initialization
	void Start () {
        player = FindObjectOfType<PlayerController>().gameObject.transform;
        playerLength = player.GetComponent<BoxCollider>().size.z;
	}
	
	// Update is called once per frame
	void Update () {
        DestroyIfOffScreen();
	}

    void OnBecameInvisible() {
        Destroy(gameObject);
    }

    private void DestroyIfOffScreen()
    {
        Vector3 positionDiff = player.position - transform.position;
        if (positionDiff.z > 0 && positionDiff.magnitude > playerLength * 5f)
        {
            Destroy(gameObject);
        }
    }
}
