﻿using UnityEngine;
using System.Collections;

public class ObstacleSpawner : MonoBehaviour {

    public GameObject asteroidPrefab;
    public GameObject spaceDebrisPrefab;
    public GameObject player;
    public GameObject game;

    public float maxScale = 1f;
    public float minScale = 0.25f; 

    private float minZ, maxZ;
    private float minY, maxY;
    public float speed = 4f; // speed of the asteroids when they're spawned
    public float armLerp = 0.1f;
    public float initialDelay = 5f;
    public float regularDelay = 1f;


    Vector3 targetVelocity;

	// Use this for initialization
	void Start () {
        //initial delay, period
        InvokeRepeating("SpawnObstacle", initialDelay, regularDelay);

        player = GameObject.Find("Player").gameObject;
        game  = GameObject.Find("GameManager").gameObject;

    }

    private void SpawnObstacle() {
        minZ = gameObject.transform.position.z - gameObject.transform.localScale.z / 2;
        maxZ = gameObject.transform.position.z + gameObject.transform.localScale.z / 2;
        minY = gameObject.transform.position.y - gameObject.transform.localScale.y / 2;
        maxY = gameObject.transform.position.y + gameObject.transform.localScale.y / 2;

        SpawnObstacle(asteroidPrefab);
    }

    private Vector3 GetRandomSpawnPosition() {
        return new Vector3(transform.position.x, Random.Range(minY, maxY), Random.Range(minZ, maxZ));
    }

    private Quaternion GetRandomRotation() {
        //return Quaternion.identity;
        return Random.rotation;
    }

    private void RandomScale(GameObject obstacle) {
        float randomScale = Random.Range(minScale, maxScale);
        obstacle.transform.localScale = new Vector3(randomScale, randomScale, randomScale);
    }

    private Vector3 GetRandomVelocity(GameObject spawnedObject) {

        Vector3 vectorTowardsPlayer = (player.transform.position - transform.position).normalized * Time.timeSinceLevelLoad/10;//time;
        return vectorTowardsPlayer;
    }

    private void SpawnObstacle(GameObject prefab) {
        //Debug.Log("Spawning obstacle " + prefab.name);
        GameObject newObstacle = Instantiate(prefab, GetRandomSpawnPosition(), GetRandomRotation()) as GameObject;
        RandomScale(newObstacle);
        newObstacle.GetComponent<Rigidbody>().velocity = GetRandomVelocity(newObstacle);
        
    }
}
