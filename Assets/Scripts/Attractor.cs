﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (Rigidbody))]
public class Attractor : MonoBehaviour {

    public string[] layersToAttract;
    public float radius = 10.0f;

    private LayerMask attractionLayerMask;
    public float gravityScale = 1f;

	// Use this for initialization
	void Start () {
        attractionLayerMask = LayerMask.GetMask(layersToAttract);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        List<Collider> objects = GetObjectsToAttract();
        AttractObjects(objects);
    }

    private void AttractObjects(List<Collider> collidersToAttract) {

        foreach (Collider collider in collidersToAttract) {
            Rigidbody rigidbody = collider.gameObject.GetComponent<Rigidbody>();
            if (rigidbody != null) {

                float distance = Vector3.Distance(transform.position, rigidbody.position);
                
                float distanceFactor = 1f / (distance * distance);

                Vector3 forceVector = (transform.position - collider.gameObject.transform.position).normalized * distanceFactor * gravityScale * Time.deltaTime;

                // Check if is a player.
                PlayerController playerController = rigidbody.GetComponent<PlayerController>();

                if (playerController != null)
                {
                    playerController.attractionForce += forceVector;
                }
                else
                {
                    rigidbody.AddForce(forceVector, ForceMode.Force);
                }
            }

        }
    }

    private List<Collider> GetObjectsToAttract() {

        List<Collider> objectsToAttract = new List<Collider>();

        // Get the objects within range
        Collider[] objectsInRange = Physics.OverlapSphere(transform.position, radius, attractionLayerMask);
        foreach (Collider collider in objectsInRange) {
            if (collider.gameObject != gameObject) {
                objectsToAttract.Add(collider);
            }
            
        }

        return objectsToAttract;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(1, 0, 0, 0.4f);
        Gizmos.DrawSphere(transform.position, radius);
    }
}
