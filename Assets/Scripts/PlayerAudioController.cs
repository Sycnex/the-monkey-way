﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerAudioController : MonoBehaviour
{
    #region Fields

    // SETTINGS //
    public List<AudioClip> impactSounds;
    public AudioClip engineSound; 

    // REFERENCES //
    public AudioSource audioSource;

    #endregion

    #region Unity Functions
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        PlayEngineSound();
    }

    private void PlayEngineSound()
    {
        audioSource.clip = engineSound;
        audioSource.PlayDelayed(0.3f);
    }

    private void OnCollisionEnter(Collision _collision)
    {
        if (_collision.gameObject.tag == "Asteroids")
        {
            audioSource.PlayOneShot(impactSounds[Random.Range(0, impactSounds.Count)]);
        }
    }

    #endregion
}
